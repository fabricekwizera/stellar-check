package main

import (
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
	"github.com/stellar/go/clients/horizonclient"
)

// Dealing with different errors in the app
func FatalLog(err error) {
	if err != nil {
		if reflect.TypeOf(err).String() != "error" {
			fmt.Println()
			log.Fatal(err.(*horizonclient.Error).Problem.Detail)
		}
		fmt.Println()
		log.Fatal(err)
	}
}
