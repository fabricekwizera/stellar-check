# STELLAR - CHECK #

## About this repo ##

Written in GoLang, this repo encovers a proof of concept of making payments on [Stellar](https://www.stellar.org/) using [Horizon API](https://www.stellar.org/developers/reference/).


## Below points were covered in the code ##

* Generate key pairs (seed and address).
* Create a sender account and fund it using Friendbot
* Create a receiver account
* Build a payment transaction, encode it and submit it to testnet.
* Print on the console new account balances.

## Sample code ##

```go

import (
	"fmt"

	"github.com/stellar/go/clients/horizonclient"
)

func main() {

	senderPair := MakeSeedAndAddr()
	senderSeed := senderPair.Seed()
	senderAddress := senderPair.Address()
	client := horizonclient.DefaultTestNetClient
	client.Fund(senderAddress)
	senderRequest := horizonclient.AccountRequest{AccountID: senderAddress}
	senderAccount, err := client.AccountDetail(senderRequest)
	FatalLog(err)
	PrintBalance(&senderRequest, client)

	receiverPair := MakeSeedAndAddr()
	receiverSeed := receiverPair.Seed()
	receiverAddress := receiverPair.Address()
	transaction := BuildTransaction(receiverAddress, senderAccount)
	SubmitTransaction(client, transaction, senderPair)
	PrintBalance(&senderRequest, client)
	receiverRequest := horizonclient.AccountRequest{AccountID: receiverAddress}
	_, errr := client.AccountDetail(receiverRequest)
	FatalLog(errr)
	PrintBalance(&receiverRequest, client)
}

```
## Running the code ##

The instructions are for Unix based OS. If you are using Windows, you will have to make searches. I assume that Go is installed on your PC, if not click [here](https://golang.org/doc/install)

### 1. Cloning the repository ###

```
% git clone https://fabricekwizera@bitbucket.org/fabricekwizera/stellar-check.git

% cd stellar-check/

```
### 2. Running the code ###

``` 
% go run *.go 
```

## Output ##

```
#####################  STELLAR - CHECK OUTPUT  ###################

Sender seed is:  SBMPYK7LMTPLTXBFKX4ISHSCPSUPIL66HMHLETANAD4MMWTNTCOYIZVL
Sender address is:  GAMYBEB5XXST774PFFF6J4D37FT6VHVBMC4CSM2PS6VJGXKQFMPVX263

***************************** Before the transaction
Balance of account: GAMYBEB5XXST774PFFF6J4D37FT6VHVBMC4CSM2PS6VJGXKQFMPVX263
INFO[0014] 10000.0000000                                

Receiver seed is:  SDN3N3MBPSV7HOKYLLIFMI2JRZ7P5PWKJPC56IPY4UO3423YJW5NULWI
Receiver address is:  GD75AEYSNU2INT3GGKTSZZHLUJDHMGT5RDIBY4CZK4HHZCDQRU2RYOQY

INFO[0018] Transaction submitted successfully           

***************************** After the transaction
Balance of account: GAMYBEB5XXST774PFFF6J4D37FT6VHVBMC4CSM2PS6VJGXKQFMPVX263
INFO[0019] 9989.9999900                                 

Balance of account: GD75AEYSNU2INT3GGKTSZZHLUJDHMGT5RDIBY4CZK4HHZCDQRU2RYOQY
INFO[0020] 10.0000000

```