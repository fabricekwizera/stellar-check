package main

import (
	"fmt"

	"github.com/stellar/go/clients/horizon"
	"github.com/stellar/go/clients/horizonclient"
	"github.com/stellar/go/keypair"
	"github.com/stellar/go/network"
	"github.com/stellar/go/txnbuild"

	log "github.com/sirupsen/logrus"
)

// Building a transaction from the address of the receiver and account of the sender and return the transaction
func BuildTransaction(receiverAddress string, senderAccount horizon.Account) txnbuild.Transaction {
	operation := txnbuild.CreateAccount{
		Destination: receiverAddress,
		Amount:      "10",
	}
	transaction := txnbuild.Transaction{
		SourceAccount: &senderAccount,
		Operations:    []txnbuild.Operation{&operation},
		Timebounds:    txnbuild.NewTimeout(300),
		Network:       network.TestNetworkPassphrase,
	}
	return transaction
}

// Encoding the transaction with base 64 and submit it onthe netwrork
func SubmitTransaction(client *horizonclient.Client, transaction txnbuild.Transaction, senderPair *keypair.Full) {
	encodedTransaction, err := transaction.BuildSignEncode(senderPair)
	FatalLog(err)
	_, errr := client.SubmitTransactionXDR(encodedTransaction)
	FatalLog(errr)
	fmt.Println()
	log.Println("Transaction submitted successfully")
}
