package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"github.com/stellar/go/clients/horizonclient"
	"github.com/stellar/go/keypair"
)

func MakeSeedAndAddr() (pair *keypair.Full) {
	seedAndAddr, err := keypair.Random()
	FatalLog(err)
	return seedAndAddr
}

func PrintBalance(request *horizonclient.AccountRequest, client *horizonclient.Client) {
	detail, err := client.AccountDetail(*request)
	FatalLog(err)
	balances := detail.Balances
	for _, balance := range balances {
		fmt.Printf("Balance of account: %s\n", detail.AccountID)
		log.Println(balance.Balance)
		fmt.Println()
	}

}
