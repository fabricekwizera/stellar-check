module bitbucket.org/fabricekwizera/stellar-check

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/stellar/go v0.0.0-20200211194036-f95820107d5f
)
