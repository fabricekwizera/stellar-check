package main

import (
	"fmt"

	"github.com/stellar/go/clients/horizonclient"
)

func main() {
	fmt.Println()
	fmt.Println("#####################  STELLAR - CHECK OUTPUT  ###################")
	fmt.Println()
	// Generating the sender's address and key and printing them on the console
	senderPair := MakeSeedAndAddr()
	senderSeed := senderPair.Seed()
	senderAddress := senderPair.Address()
	fmt.Println("Sender seed is: ", senderSeed)
	fmt.Println("Sender address is: ", senderAddress)

	//  With use of Friendbot, creating an address and funding it
	client := horizonclient.DefaultTestNetClient
	client.Fund(senderAddress)

	// Printing on the console the balance of the sender account
	senderRequest := horizonclient.AccountRequest{AccountID: senderAddress}
	senderAccount, err := client.AccountDetail(senderRequest)
	FatalLog(err)
	fmt.Println("\n***************************** Before the transaction")
	PrintBalance(&senderRequest, client)

	// Generating the receiver's address and key and printing them on the console
	receiverPair := MakeSeedAndAddr()
	receiverSeed := receiverPair.Seed()
	receiverAddress := receiverPair.Address()
	fmt.Println("Receiver seed is: ", receiverSeed)
	fmt.Println("Receiver address is: ", receiverAddress)

	// Building, encoding a transaction and submitting it to the network
	transaction := BuildTransaction(receiverAddress, senderAccount)
	SubmitTransaction(client, transaction, senderPair)

	fmt.Println("\n***************************** After the transaction")
	PrintBalance(&senderRequest, client)
	receiverRequest := horizonclient.AccountRequest{AccountID: receiverAddress}
	_, errr := client.AccountDetail(receiverRequest)
	FatalLog(errr)
	PrintBalance(&receiverRequest, client)
}
